from shared/variables import *
from rhel/variables import *
from shared/runroot import *
from shared/pkgset import *
from shared/createrepo import *
from shared/createiso import *
from shared/general import extra_files

# PRODUCT INFO
release_name = "Supplementary"
release_short = "Supp"
release_version = RELEASE_VERSION
release_is_layered = True

base_product_name = "Red Hat Enterprise Linux"
base_product_short = "RHEL"
base_product_version = "9"


# GENERAL SETTINGS
comps_file = {
    "scm": "git",
    "repo": "https://gitlab.cee.redhat.com/rhel/release-engineering/comps.git",
    "file": "comps-rhel-%s-supplementary.xml"  % RELEASE_VERSION_XYZ,
    "branch": "rhel-9",
    "command": "make comps-rhel-%s-supplementary.xml"  % RELEASE_VERSION_XYZ,
}

# Local variant file referenced via git is intentional.
# This way, when config is dumped by pungi-config-dump, the branch
# reference will be expanded to the current commit hash, then when
# such expanded config is used in future, it will consume the exactly
# same variant file again (ignoring all changes that landed later).
variants_file = {
    "scm": "git",
    "repo": "https://gitlab.cee.redhat.com/rhel/release-engineering/pungi-redhat.git",
    "branch": "rhel-%s" % RELEASE_VERSION_XYZ,
    "file": "variants-supplementary.xml",
}

sigkeys = ["FD431D51", "F21541EB", None] # None = unsigned

multilib = [
    ("^.*$", {
        "*": ["devel", "runtime"]
    })
]

# RUNROOT settings
runroot_channel = RUNROOT_CHANNEL
runroot_tag = RUNROOT_TAG

# PKGSET - KOJI
pkgset_koji_tag = "supp-rhel-%s-z-compose"  % RELEASE_VERSION_XYZ

# GATHER
gather_source = "comps"
gather_method = "deps"
check_deps = False
greedy_method = "build"

# fomat: [(variant_uid_regex, {arch|*: [repos]})]
gather_lookaside_repos = [
    ("^.*$", {
        "aarch64": [
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/aarch64/os" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/source/tree/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/aarch64/os/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/source/tree/" % RELEASE_VERSION_XYZ,
        ],
        "ppc64le": [
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/ppc64le/os" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/source/tree/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/ppc64le/os/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/source/tree/" % RELEASE_VERSION_XYZ,
        ],
        "s390x": [
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/s390x/os" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/source/tree/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/s390x/os/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/source/tree/" % RELEASE_VERSION_XYZ,
        ],
        "x86_64": [
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/x86_64/os" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/AppStream/source/tree/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/x86_64/os/" % RELEASE_VERSION_XYZ,
            "file:///mnt/odcs/prod/production/latest-RHEL-%s/compose/BaseOS/source/tree/" % RELEASE_VERSION_XYZ,
        ],
    }),
]

filter_packages = [
    # remove unwanted sources
    ("^.*$", {
        "*": [
        ],
    }),
]

additional_packages = [
    ("^Supplementary$", {
        "*": [
            "zhongyi-song-fonts",
        ]
    })
]

createiso_break_hardlinks = True

cts_url = "https://cts.engineering.redhat.com/"
cts_keytab = "/etc/odcs/odcs.keytab"
